/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Base;

import java.sql.*;
import Clases.Usuario;

/**
 *
 * @author miker
 */
public class DeleteAdmin extends Conexion {
    
    public boolean delete(Usuario user){
        PreparedStatement into = null;
        Connection conectar = getConexion();
        
        String query = "DELETE FROM usuarioadmin WHERE id=?";
        
        try {
            into = conectar.prepareStatement(query);
            into.setInt(1, user.getId());
            into.execute();
            return true;
        } catch (Exception e) {
        
            System.out.println("Error " + e);
            return false;
        } finally {
            try {
                conectar.close();
            } catch (Exception e) {
                System.out.println("Error " + e);
            }
        }
    }
    
}
