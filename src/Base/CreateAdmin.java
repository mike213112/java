/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Base;

import java.sql.*;
import Clases.Usuario;
/**
 *
 * @author miker
 */
public class CreateAdmin extends Conexion {
    
    public boolean ingresar(Usuario user){
        PreparedStatement into = null;
        Connection conectar = getConexion();
        
        String query = "INSERT INTO usuarioadmin (nombre,apellido,usuario,contra,fecha_de_nacimiento,edad,foto) values(?,?,?,?,?,?,?)";
        
        try {
            into = conectar.prepareStatement(query);
            into.setString(1, user.getNombre());
            into.setString(2, user.getApellido());
            into.setString(3, user.getUser());
            into.setString(4, user.getPass());
            into.setString(5, user.getFecha_de_nacimiento());
            into.setInt(6, user.getEdad());
            into.setBytes(7, user.getFoto());
            into.execute();
            return true;
        } catch (Exception e) {
            System.out.println("Error " + e);
            return false;
        } finally {
            try {
                conectar.close();
            } catch (Exception e) {
                System.out.println("Error " + e);
            }
        }
    }
    
}
