/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it0.pkg1;

import interfaces.Elegir;
import Base.*;
import Admin.AddUserAdmin;
import Admin.Principal;
import Clases.Usuario;
import Controller.*;

/**
 *
 * @author Danny Cáceres
 */
public class IT01 {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Usuario user = new Usuario();
        AddUserAdmin admin = new AddUserAdmin();
        CreateAdmin c = new CreateAdmin();
        ReadAdmin r = new ReadAdmin();
        UpdateAdmin u = new UpdateAdmin();
        DeleteAdmin d = new DeleteAdmin();
        Principal p = new Principal();
        
        //ControllerAdmin controladmin = new ControllerAdmin(user, c, admin);
        p.setVisible(true);
    }
    
}
